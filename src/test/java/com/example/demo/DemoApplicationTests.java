package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;

import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    void contextLoads() {
        String queryString = " "+"select * from users t1 join users t2 on t1.id = t2.id";
        queryString = queryString.replaceAll(" +"," ");
        queryString = queryString.replaceAll("\\( "," ");
        queryString = queryString.replaceAll("[^(][sS][eE][lL][eE][cC][tT] [a-zA-Z0-9._, )(*]+ [fF][rR][oO][mM]","SELECT COUNT(1) as count FROM");
        System.out.println(queryString);
        Map<String, Object> map =  jdbcTemplate.queryForMap(queryString);
        assertThat(map.get("count").toString()).isEqualTo("27");
    }

}
